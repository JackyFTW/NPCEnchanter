package me.jackyftw.enchanter.gui;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static me.jackyftw.enchanter.utils.Utils.color;

public class ToolGUI {

    private String guiName;
    private Material[] materials;
    private List<Enchant> enchantCategories = new ArrayList<>();
    private Inventory inv;

    public ToolGUI(String guiName, Material... materials) {
        this.guiName = color(guiName);
        this.materials = materials;

        // Close item
        this.inv = Bukkit.createInventory(null, 27, this.guiName);
        ItemStack close = new ItemStack(Material.BARRIER, 1);
        ItemMeta closeMeta = close.getItemMeta();
        closeMeta.setDisplayName(color("&cClose"));
        closeMeta.setLore(new ArrayList<>(Arrays.asList(
                " ",
                color("&3Click to close the GUI!"),
                " "
        )));
        close.setItemMeta(closeMeta);
        inv.setItem(22, close);

        compileItems();
    }

    public void compileItems() {
        // Used for compiling the items for each instance of this class
    }

    /*
      Getting
     */

    public List<Material> getMaterials() {
        return new ArrayList<>(Arrays.asList(materials));
    }

    public List<Enchant> getEnchantCategories() {
        return enchantCategories;
    }

    public String getGuiName() {
        return guiName;
    }

    public Inventory getInventory() {
        return inv;
    }

    /*
      Setting
     */

    public void addEnchantCategory(Enchant enchant) {
        enchantCategories.add(enchant);
    }

    public void setInventory(Inventory inv) {
        this.inv = inv;
    }
}
