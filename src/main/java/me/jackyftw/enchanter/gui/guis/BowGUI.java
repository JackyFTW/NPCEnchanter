package me.jackyftw.enchanter.gui.guis;

import me.jackyftw.enchanter.enchant.Enchant;
import me.jackyftw.enchanter.enchant.EnchantManager;
import me.jackyftw.enchanter.gui.ToolGUI;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

import static me.jackyftw.enchanter.utils.Utils.color;

public class BowGUI extends ToolGUI {

    public BowGUI() {
        super("&dBow Enchantments", Material.BOW);
    }

    @Override
    public void compileItems() {
        Inventory inv = getInventory();

        addEnchantCategory(EnchantManager.getEnchant("Flame"));
        addEnchantCategory(EnchantManager.getEnchant("Infinity"));
        addEnchantCategory(EnchantManager.getEnchant("Power"));
        addEnchantCategory(EnchantManager.getEnchant("Punch"));
        addEnchantCategory(EnchantManager.getEnchant("Unbreaking"));

        for(Enchant enchant : getEnchantCategories()) {
            String formattedCategoryName = enchant.getCategoryName().replaceAll("-", " ");

            ItemStack item = new ItemStack(Material.ENCHANTED_BOOK, 1);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(color("&d" + formattedCategoryName));
            itemMeta.setLore(new ArrayList<>(Arrays.asList(
                    " ",
                    color("&aClick to view all " + formattedCategoryName + " enchantments!"),
                    " "
            )));
            item.setItemMeta(itemMeta);

            inv.addItem(item);
        }
        setInventory(inv);
    }

}
