package me.jackyftw.enchanter.gui.guis;

import me.jackyftw.enchanter.enchant.Enchant;
import me.jackyftw.enchanter.enchant.EnchantManager;
import me.jackyftw.enchanter.gui.ToolGUI;
import static me.jackyftw.enchanter.utils.Utils.color;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

public class PickaxeGUI extends ToolGUI {

    public PickaxeGUI() {
        super("&dPickaxe Enchantments", Material.WOOD_PICKAXE, Material.STONE_PICKAXE, Material.GOLD_PICKAXE, Material.IRON_PICKAXE, Material.DIAMOND_PICKAXE);
    }

    @Override
    public void compileItems() {
        Inventory inv = getInventory();

        addEnchantCategory(EnchantManager.getEnchant("Unbreaking"));
        addEnchantCategory(EnchantManager.getEnchant("Fortune"));
        addEnchantCategory(EnchantManager.getEnchant("Efficiency"));
        addEnchantCategory(EnchantManager.getEnchant("Silk-Touch"));

        for(Enchant enchant : getEnchantCategories()) {
            String formattedCategoryName = enchant.getCategoryName().replaceAll("-", " ");

            ItemStack item = new ItemStack(Material.ENCHANTED_BOOK, 1);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(color("&d" + formattedCategoryName));
            itemMeta.setLore(new ArrayList<>(Arrays.asList(
                    " ",
                    color("&aClick to view all " + formattedCategoryName + " enchantments!"),
                    " "
            )));
            item.setItemMeta(itemMeta);

            inv.addItem(item);
        }
        setInventory(inv);
    }

}