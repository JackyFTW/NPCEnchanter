package me.jackyftw.enchanter.gui.guis;

import me.jackyftw.enchanter.enchant.Enchant;
import me.jackyftw.enchanter.enchant.EnchantManager;
import me.jackyftw.enchanter.gui.ToolGUI;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

import static me.jackyftw.enchanter.utils.Utils.color;

public class ShovelGUI extends ToolGUI {

    public ShovelGUI() {
        super("&dShovel Enchantments", Material.WOOD_SPADE, Material.STONE_SPADE, Material.GOLD_SPADE, Material.IRON_SPADE, Material.DIAMOND_SPADE);
    }

    @Override
    public void compileItems() {
        Inventory inv = getInventory();

        addEnchantCategory(EnchantManager.getEnchant("Unbreaking"));
        addEnchantCategory(EnchantManager.getEnchant("Fortune"));
        addEnchantCategory(EnchantManager.getEnchant("Efficiency"));
        addEnchantCategory(EnchantManager.getEnchant("Silk-Touch"));

        for(Enchant enchant : getEnchantCategories()) {
            String formattedCategoryName = enchant.getCategoryName().replaceAll("-", " ");

            ItemStack item = new ItemStack(Material.ENCHANTED_BOOK, 1);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(color("&d" + formattedCategoryName));
            itemMeta.setLore(new ArrayList<>(Arrays.asList(
                    " ",
                    color("&aClick to view all " + formattedCategoryName + " enchantments!"),
                    " "
            )));
            item.setItemMeta(itemMeta);

            inv.addItem(item);
        }
        setInventory(inv);
    }

}
