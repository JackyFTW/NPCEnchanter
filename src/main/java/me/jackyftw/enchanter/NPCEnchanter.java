package me.jackyftw.enchanter;

import me.jackyftw.enchanter.enchant.EnchantManager;
import me.jackyftw.enchanter.file.FileManager;
import me.jackyftw.enchanter.gui.ToolGUIManager;
import me.jackyftw.enchanter.listeners.InventoryListener;
import me.jackyftw.enchanter.listeners.NpcListener;
import me.jackyftw.enchanter.listeners.PlayerListener;
import me.jackyftw.enchanter.npc.NpcManager;
import me.jackyftw.enchanter.utils.Utils;
import net.citizensnpcs.api.CitizensAPI;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public class NPCEnchanter extends JavaPlugin {

    public static NPCEnchanter instance;
    public static Economy economy = null;

    @Override
    public void onEnable() {
        instance = this;

        FileManager.setup(); // Sets up all files and data

        EnchantManager.registerEnchants(); // Adds and registers all enchants

        ToolGUIManager.registerToolGUIS(); // Handles the tool GUIs & sets it up

        // Creates and sets data for the NPC based off of config
        NpcManager.registerNpcs();
        NpcManager.spawnNpcs();
        NpcManager.setSkins();

        registerListeners();

        if(!setupEconomy()) {
            Utils.log(Level.SEVERE, "Disabling since no Vault plugin was found...");
            getServer().getPluginManager().disablePlugin(this);
        }
    }

    @Override
    public void onDisable() {
        instance = null;

        // Deletes npc
        if(NpcManager.getEnchanter().isSpawned()) {
            NpcManager.getEnchanter().despawn();
        }
        CitizensAPI.getNPCRegistry().deregister(NpcManager.getEnchanter());
    }

    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new NpcListener(), this);
        pm.registerEvents(new InventoryListener(), this);
        pm.registerEvents(new PlayerListener(), this);
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        economy = rsp.getProvider();
        return economy != null;
    }
    

}
