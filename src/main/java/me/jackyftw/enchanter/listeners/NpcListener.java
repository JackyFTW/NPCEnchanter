package me.jackyftw.enchanter.listeners;

import me.jackyftw.enchanter.enchant.EnchantManager;
import me.jackyftw.enchanter.gui.ToolGUIManager;
import net.citizensnpcs.api.event.NPCLeftClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class NpcListener implements Listener {

    public NpcListener() { }

    @EventHandler
    public void onRightClick(NPCRightClickEvent e) {
        NPC npc = e.getNPC();
        Player p = e.getClicker();
        if(npc.getName().contains("Enchanter")) {
            ToolGUIManager.openInventory(p);
        }
    }

    @EventHandler
    public void onLeftClick(NPCLeftClickEvent e) {
        NPC npc = e.getNPC();
        Player p = e.getClicker();
        if(npc.getName().contains("Enchanter")) {
            ToolGUIManager.openInventory(p);
        }
    }

}
