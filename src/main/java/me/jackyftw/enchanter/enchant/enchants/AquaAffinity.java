package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class AquaAffinity extends Enchant {

    public AquaAffinity() {
        super("Aqua-Affinity", Enchantment.WATER_WORKER);
    }

}
