package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Fortune extends Enchant {

    public Fortune() {
        super("Fortune", Enchantment.LOOT_BONUS_BLOCKS);
    }

}
