package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Knockback extends Enchant {

    public Knockback() {
        super("Knockback", Enchantment.KNOCKBACK);
    }

}
