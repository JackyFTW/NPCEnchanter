package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Lure extends Enchant {

    public Lure() {
        super("Lure", Enchantment.LURE);
    }

}
