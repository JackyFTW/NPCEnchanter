package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Looting extends Enchant {

    public Looting() {
        super("Looting", Enchantment.LOOT_BONUS_MOBS);
    }

}
