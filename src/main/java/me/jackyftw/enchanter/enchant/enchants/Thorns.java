package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Thorns extends Enchant {

    public Thorns() {
        super("Thorns", Enchantment.THORNS);
    }

}
