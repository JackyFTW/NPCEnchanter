package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class FireAspect extends Enchant {

    public FireAspect() {
        super("Fire-Aspect", Enchantment.FIRE_ASPECT);
    }

}
