package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Punch extends Enchant {

    public Punch() {
        super("Punch", Enchantment.ARROW_KNOCKBACK);
    }

}
