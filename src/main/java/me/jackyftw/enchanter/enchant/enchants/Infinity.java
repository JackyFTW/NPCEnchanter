package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class Infinity extends Enchant {

    public Infinity() {
        super("Infinity", Enchantment.ARROW_INFINITE);
    }

}
