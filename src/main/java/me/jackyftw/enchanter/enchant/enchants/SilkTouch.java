package me.jackyftw.enchanter.enchant.enchants;

import me.jackyftw.enchanter.enchant.Enchant;
import org.bukkit.enchantments.Enchantment;

public class SilkTouch extends Enchant {

    public SilkTouch() {
        super("Silk-Touch", Enchantment.SILK_TOUCH);
    }

}
