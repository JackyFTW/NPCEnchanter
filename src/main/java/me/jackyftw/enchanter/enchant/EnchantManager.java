package me.jackyftw.enchanter.enchant;

import me.jackyftw.enchanter.enchant.enchants.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EnchantManager {

    private static List<Enchant> enchants = new ArrayList<>();
    private static HashMap<String, String> aliases = new HashMap<>();

    public static List<Enchant> getEnchants() {
        return enchants;
    }

    public static void registerEnchants() {
        enchants.add(new Unbreaking());
        enchants.add(new Fortune());
        enchants.add(new Efficiency());
        enchants.add(new SilkTouch());
        enchants.add(new FireAspect());
        enchants.add(new Sharpness());
        enchants.add(new BaneOfArthropods());
        enchants.add(new Knockback());
        enchants.add(new Looting());
        enchants.add(new Smite());
        enchants.add(new AquaAffinity());
        enchants.add(new BlastProtection());
        enchants.add(new FireProtection());
        enchants.add(new ProjProtection());
        enchants.add(new Protection());
        enchants.add(new Respiration());
        enchants.add(new Thorns());
        enchants.add(new DepthStrider());
        enchants.add(new FeatherFalling());
        enchants.add(new Flame());
        enchants.add(new Infinity());
        enchants.add(new Power());
        enchants.add(new Punch());
        enchants.add(new LuckOfTheSea());
        enchants.add(new Lure());

        registerAliases();
    }

    private static void registerAliases() {
        aliases.put("UNBREAKING", "DURABILITY");
        aliases.put("FORTUNE", "LOOT_BONUS_BLOCKS");
        aliases.put("EFFICIENCY", "DIG_SPEED");
        aliases.put("SILK TOUCH", "SILK_TOUCH");
        aliases.put("FIRE ASPECT", "FIRE_ASPECT");
        aliases.put("SHARPNESS", "DAMAGE_ALL");
        aliases.put("BANE OF ARTHROPODS", "DAMAGE_ARTHROPODS");
        aliases.put("KNOCKBACK", "KNOCKBACK");
        aliases.put("LOOTING", "LOOT_BONUS_MOBS");
        aliases.put("SMITE", "DAMAGE_UNDEAD");
        aliases.put("AQUA AFFINITY", "WATER_WORKER");
        aliases.put("BLAST PROTECTION", "PROTECTION_EXPLOSIONS");
        aliases.put("FIRE PROTECTION", "PROTECTION_FIRE");
        aliases.put("PROJECTILE PROTECTION", "PROTECTION_PROJECTILE");
        aliases.put("PROTECTION", "PROTECTION_ENVIRONMENTAL");
        aliases.put("RESPIRATION", "OXYGEN");
        aliases.put("THORNS", "THORNS");
        aliases.put("DEPTH STRIDER", "DEPTH_STRIDER");
        aliases.put("FEATHER FALLING", "PROTECTION_FALL");
        aliases.put("FLAME", "ARROW_FIRE");
        aliases.put("INFINITY", "ARROW_INFINITE");
        aliases.put("POWER", "ARROW_DAMAGE");
        aliases.put("PUNCH", "ARROW_KNOCKBACK");
        aliases.put("LUCK OF THE SEA", "LUCK");
        aliases.put("LURE", "LURE");
    }

    public static Enchant getEnchant(String name) {
        for(Enchant enchant : getEnchants()) {
            if(enchant.getCategoryName().equalsIgnoreCase(name)) {
                return enchant;
            }
        }
        return null;
    }

    public static void enchant(ItemStack item, Enchantment enchant, int tier) {
        item.addEnchantment(enchant, tier);
    }

    public static Enchantment getAliasEnchant(String string) {
        String name = string.toUpperCase();
        if(aliases.containsKey(name)) {
            return Enchantment.getByName(aliases.get(name));
        }
        return null;
    }

}
